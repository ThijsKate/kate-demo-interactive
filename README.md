# KATE Demo Interactive

![DEMO example](example.png)

This demo is set up to work without any configuration for demo.kateinnovations.com or localhost:3000 if you have react running on your device.

## Update Iframe URL

1. Copy and paste the local.html file to a new file
2. Change the iframe src to your preferred URL

## Editing Background image

1. Copy and paste the local.html file to a new file
2. Change the img src to your file
3. Update the transform css property so it fits your background image nicely

TODO:
- Remove css from head of file to its own file